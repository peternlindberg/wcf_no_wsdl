﻿using System;
using Service;
using Contracts;

namespace Host
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var svc = new EasyWCF.GenericHost<ManualService, IManualService>(null, false))
            {
                Console.WriteLine("service running...");
                Console.ReadKey();
            }
        }
    }
}
