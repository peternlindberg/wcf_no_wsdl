﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Host
{
    class GenericHost<T, I> : IDisposable
    {
        ServiceHost svc;

        public GenericHost()
        {
            svc = new ServiceHost(typeof(T));
            var binding = new NetNamedPipeBinding();
            var endpoint = "net.pipe://localhost/" + typeof(I).Name;
            svc.AddServiceEndpoint(typeof(I), binding, endpoint);
            svc.Open();
        }

        public void Dispose()
        {
            svc.Close();
        }
    }
}
