﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ServiceProxy
{
    public class ProxyFactory
    {
        public static T GetProxy<T>(string addr)
        {
            var factory = new ChannelFactory<T>(new NetNamedPipeBinding(), addr);
            return factory.CreateChannel();
        }

        public static T GetProxy<T>()
        {
            var typename = typeof(T).Name;
            var addr = "net.pipe://localhost/" + typename;
            return GetProxy<T>(addr);
        }
    }
}
