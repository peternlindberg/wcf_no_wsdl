﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using Contracts;

namespace ServiceProxy
{
    [Obsolete]
    public class Proxy : IManualService
    {
        private IManualService channel;

        public Proxy()
        {
            var factory = new ChannelFactory<IManualService>(new NetNamedPipeBinding(), "net.pipe://localhost/ManualService");
            channel = factory.CreateChannel();
        }

        public string GetData()
        {
            return channel.GetData();
        }
    }
}
