﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace EasyWCF
{
    public class GenericHost<T, I> : IDisposable
    {
        ServiceHost svc;

        /// <summary>
        /// Create a host for a WCF service, based on it's Interface and Implementation
        /// </summary>
        /// <param name="useTcp">use netTcpBinding if true, otherwise use netNamedPipe</param>
        public GenericHost(String hostname, bool useTcp = true)
        {
            svc = new ServiceHost(typeof(T));
            Binding binding;
            String scheme;
            if (useTcp)
            {
                binding = new NetTcpBinding();
                scheme = "net.tcp";
            }
            else
            { 
                binding = new NetNamedPipeBinding();
                scheme = "net.pipe";
            }

            hostname = hostname ?? "localhost";

            var endpoint = scheme + "://" + hostname + "/" + typeof(I).Name;
            svc.AddServiceEndpoint(typeof(I), binding, endpoint);
            svc.Open();
        }

        public void Dispose()
        {
            svc.Close();
        }
    }
}
