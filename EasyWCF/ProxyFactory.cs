﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace EasyWCF
{
    public class ProxyFactory
    {

        public static T GetProxy<T>(String hostname, bool useTcp = true)
        {
            hostname = hostname ?? "localhost";

            Binding binding;
            String scheme;
            if (useTcp)
            {
                binding = new NetTcpBinding();
                scheme = "net.tcp";
            }
            else
            {
                var nnpbinding = new NetNamedPipeBinding();
                //transfer mode can be set to streaming if large blobs are frequently transferred
                //nnpbinding.TransferMode = TransferMode.Buffered;
                binding = nnpbinding;
                scheme = "net.pipe";
            }

            var addr = scheme + "://" + hostname + "/" + typeof(T).Name;
            var factory = new ChannelFactory<T>(binding, addr);
            return factory.CreateChannel();
        }

    }
}
