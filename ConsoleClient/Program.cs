﻿using System;
using Contracts;

namespace ConsoleClient
{
    class Program
    {
        static void Main(string[] args)
        {
            //proxy will use NetNamedPipesBinding
            var proxy = EasyWCF.ProxyFactory.GetProxy<IManualService>(null, false);
            var msg = proxy.GetData();

            Console.WriteLine(msg);
            Console.ReadKey();
        }
    }
}
