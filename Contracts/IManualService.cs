﻿using System.ServiceModel;

namespace Contracts
{
    /// <summary>
    /// Both Client and Server must have access to this
    /// </summary>
    [ServiceContract]
    public interface IManualService
    {
        [OperationContract]
        string GetData();
    }
}
