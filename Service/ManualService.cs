﻿using Contracts;

namespace Service
{
    /// <summary>
    /// the service implementation -- onlyt the server end needs access to this
    /// </summary>
    public class ManualService : IManualService
    {
        public string GetData()
        {
            System.Console.WriteLine("GetData was called by a client");
            return "hello";
        }
    }
}
